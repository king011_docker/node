#!/usr/bin/env bash
set -ex
Dirname=$(cd $(dirname $BASH_SOURCE) && pwd)
cd "$Dirname"

sudo docker build -t king011/node:14.17.0 .