#!/bin/bash
#Program:
#       Angular CLI for bash completion
#History:
#       2018-06-11 king011 first release
#Email:
#       zuiwuchang@gmail.com

function _king011_ng_basic_()
{
    COMPREPLY=($(compgen -W 'new update build serve xi18n extract-i18n generate version help' -- "$word"))
}
function _king011_ng_new_()
{
    if [ 2 == $COMP_CWORD ];then
        # 沒有輸入任務名稱 
        COMPREPLY=($(compgen -A file -- "$word"))
    else
        # 補齊參數
        COMPREPLY=($(compgen -W '-h --collection -c --directory --dry-run -d --force -f \
        --inline-style -s --inline-template -t --new-project-root --prefix -p --routing --skip-git -g \
        --skip-install --skip-tests -S --style --verbose -v --view-encapsulation \
        ' -- "$word"))
    fi
}
function _king011_ng_serve_()
{
    COMPREPLY=($(compgen -W '-h --aot --base-href --browser-target --common-chunk --configuration -c \
        --deploy-url --disable-host-check --eval-source-map --hmr --hmr-warning --host --live-reload \
        --open -o --optimization --poll --port --prod --progress --proxy-config --public-host \
        --serve-path --serve-path-default-warning --source-map --ssl --ssl-cert --ssl-key \
        --vendor-chunk --verbose --watch \
        ' -- "$word"))
}
function _king011_ng_generate_class_()
{
    if [ 3 == $COMP_CWORD ];then
        COMPREPLY=($(compgen -A file -- "$word"))
    else
        COMPREPLY=($(compgen -W '-h --dry-run -d --force -f --project --spec --type \
        ' -- "$word"))
    fi
}
function _king011_ng_generate_component_()
{
    if [ 3 == $COMP_CWORD ];then
        COMPREPLY=($(compgen -A file -- "$word"))
    else
        COMPREPLY=($(compgen -W '-h --change-detection -c --dry-run -d --export --flat --force -f \
        --inline-style -s --inline-template -t --module -m --prefix -p --project --selector --skip-import \
        --spec --styleext --view-encapsulation -v \
        ' -- "$word"))
    fi
}
function _king011_ng_generate_directive_()
{
    if [ 3 == $COMP_CWORD ];then
        COMPREPLY=($(compgen -A file -- "$word"))
    else
        COMPREPLY=($(compgen -W '-h --dry-run -d --export --flat --force -f --module -m --prefix -p \
        --project --selector --skip-import --spec \
        ' -- "$word"))
    fi
}
function _king011_ng_generate_module_()
{
    if [ 3 == $COMP_CWORD ];then
        COMPREPLY=($(compgen -A file -- "$word"))
    else
        COMPREPLY=($(compgen -W '-h --dry-run -d --flat --force -f --module -m \
        --project --routing --routing-scope --spec \
        ' -- "$word"))
    fi
}
function _king011_ng_generate_service_()
{
    if [ 3 == $COMP_CWORD ];then
        COMPREPLY=($(compgen -A file -- "$word"))
    else
        COMPREPLY=($(compgen -W '-h --dry-run -d --flat --force -f --project --spec \
        ' -- "$word"))
    fi
}
function _king011_ng_generate_()
{
    if [ 2 == $COMP_CWORD ];then
        COMPREPLY=($(compgen -W '-h serviceWorker application class component directive \
        enum guard interface module pipe service universal appShell library \
        ' -- "$word"))
    else
        # ng generate 子命令
        case ${COMP_WORDS[2]} in
            'class')
                _king011_ng_generate_class_
            ;;

            'component')
                _king011_ng_generate_component_
            ;;

            'directive')
                _king011_ng_generate_directive_
            ;;

            'module')
                _king011_ng_generate_module_
            ;;

            'service')
                _king011_ng_generate_service_
            ;;

            *)	# default
                COMPREPLY=()
            ;;
        esac
    fi
}



function _king011_ng_()
{
    # 獲取 正在輸入的 參數
    local word=${COMP_WORDS[COMP_CWORD]}

    # switch 輸入第 幾個參數
    if [ 1 == $COMP_CWORD ];then
        _king011_ng_basic_
    else
        # ng 子命令
        case ${COMP_WORDS[1]} in
            'new')
                _king011_ng_new_
            ;;
        
            'serve')
                _king011_ng_serve_
            ;;

            'generate')
                _king011_ng_generate_
            ;;
        
            *)	# default
                COMPREPLY=()
            ;;
        esac
        
    fi
}
complete -F _king011_ng_ ng
