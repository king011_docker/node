#!/bin/bash

function self_chown
{
    if [[ -d "$1" ]];then
        chown dev.dev "$1"
    fi
}
if [[ "$@" == "watch-default" ]];then
    set -e
    if [[ -f /project.iso ]];then
        mount -o loop /project.iso /home/dev/project
    fi
    self_chown /home/dev/lib
    self_chown /home/dev/project
    self_chown /home/dev/.cache
    /opt/watch/watch -conf /opt/watch/list.json
else
    exec "$@"
fi