# golang

node development environment

This is a node integrated development environment (linux amd64) packaged with docker, which is used to quickly build a node environment that can break through the network blockade of West Korea

* because the Internet blockade in West Korea comes with a set of circumvention tools(v2ray-web proxychains iptables)
* some web testing tools (net-tools iputils-ping dnsutils ca-certificates)
* installed: vim openssh-server (if you want to develop remotely through vim)
* installed: nginx
* installed: yarn angular typescript
* installed: [code-server](https://github.com/cdr/code-server) and plug(angular)

# tag

|tag|current|
|--|--|
|14.17.0|
|16.13.2|✓|

# run

Run the container:
```
docker run \
    -v Your_Project:/home/dev/project \
    -v Your_Lib:/home/dev/lib \
    -v Your_Cache:/home/dev/.cache \
    -v Your_NPM_Cache:/home/dev/.npm \
    -p 8080:80/tcp \
    -p 10022:22/tcp \
    --name node \
    -d king011/node:16.13.2
```

* port 22 for ssh
* port 80 for v2ray-web and code-server

Nginx is deployed as a web proxy before v2ray-web and code-server. Nginx diverts **v2ray.node.docker** to v2ray-web, and diverts **code.node.docker** to code-server. So you should modify the hosts file of the host to access.

```
sudo 127.0.0.1 v2ray.node.docker >> /etc/hosts
sudo 127.0.0.1 code.node.docker >> /etc/hosts
```
# iptables
If you are in a place similar to West Korea, you need to use iptables to break through the network plus **--cap-add=NET_ADMIN --cap-add=NET_RAW**. This will allow the use of iptables in the container so that the container can use a global proxy to access the network without affecting the host.

Run the container and use iptables:
```
docker run \
    --cap-add=NET_ADMIN  --cap-add=NET_RAW \
    -v Your_Project:/home/dev/project \
    -v Your_Lib:/home/dev/lib \
    -v Your_Cache:/home/dev/.cache \
    -v Your_NPM_Cache:/home/dev/.npm \
    -p 8080:80/tcp \
    -p 10022:22/tcp \
    --name node \
    -d king011/node:16.13.2
```